import { Component, OnInit, Input } from '@angular/core';
import { Article } from './article.model';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  @Input() article: Article;

  constructor() {
  }

  public upvote = () => {
    this.article.changeVote(true);
    return false;
  }

  public downvote = () => {
    this.article.changeVote(false);
    return false;
  }

  ngOnInit() {
  }

}
