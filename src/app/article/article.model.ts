export class Article {
    public title: string;
    public link: string;
    public votes: number;

    constructor(title: string, link: string, votes?: number) {
        this.title = title;
        this.link = link;
        this.votes = votes || 0;
    }

    public changeVote = (up: boolean) => {
        if(up) this.votes++;
        else this.votes--;
    }

    public getUrlDomain = () => {
        try {
            // e.g. http://foo.com/path/to/bar
            const domainAndPath: string = this.link.split('//')[1];
            // e.g. foo.com/path/to/bar
            return domainAndPath.split('/')[0];
          } catch (err) {
            return null;
          }
    }
}