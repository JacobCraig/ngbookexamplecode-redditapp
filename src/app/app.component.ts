import { Component } from '@angular/core';
import { Article } from './article/article.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public articles: Article[] = [];
  public myForm: FormGroup;

  constructor(FormBuilder: FormBuilder) {
    this.myForm = FormBuilder.group({
      'title': ['', Validators.compose([
        Validators.required, 
        Validators.minLength(5)])],
      'link': ['', Validators.compose([
        Validators.required, 
        Validators.pattern(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)])],
    });

    this.articles.push(new Article('Google', 'https://www.google.com/'));
  }

  public addArticle = () => {
    this.articles.push(new Article(this.myForm.controls['title'].value, this.myForm.controls['link'].value));
    this.myForm.reset();
  }

  sortedArticles = () => {
    return this.articles.sort((a: Article, b: Article) => b.votes - a.votes);
  }
}
